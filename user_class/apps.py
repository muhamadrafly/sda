from django.apps import AppConfig


class UserClassConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'user_class'
