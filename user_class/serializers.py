from rest_framework import serializers
from .models import Class, UserClass, Attendance

from user.models import User
from user.serializers import UserSerializer
from course.models import Course


class ClassCourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = '__all__'


class ClassUserSerializer(serializers.ModelSerializer):
    student = UserSerializer()

    class Meta:
        model = UserClass
        fields = '__all__'


class ClassSerializer(serializers.ModelSerializer):
    class_type = serializers.CharField()
    code = serializers.CharField(read_only=True)

    class Meta:
        model = Class
        fields = ('id', 'class_type',  'code')

    def to_representation(self, instance):
        course = Course.objects.filter(_class=instance.id)
        course_serializer = ClassCourseSerializer(instance=course, many=True)
        student = UserClass.objects.filter(_class=instance.id)
        student_serializer = ClassUserSerializer(instance=student, many=True)
        return {
            'id': instance.id,
            'class_type': instance.class_type,
            'code': instance.code,
            'course': course_serializer.data,
            'user_class': student_serializer.data
        }


class UserClassSerializer(serializers.ModelSerializer):
    student = UserSerializer()
    _class = ClassSerializer()

    class Meta:
        model = UserClass
        fields = ('id', 'student', '_class')


class UserClassCreateUpdateSerializer(serializers.ModelSerializer):
    student = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.all()
    )
    _class = serializers.PrimaryKeyRelatedField(
        queryset=Class.objects.all()
    )

    class Meta:
        model = UserClass
        fields = ('id', 'student', '_class')

    def __init__(self, instance=None, data=..., **kwargs):
        super().__init__(instance, data, **kwargs)
        self.user_class = None

    def validate(self, attrs):
        self.user_class = UserClass.objects.filter(
            student=attrs.get('student').id,
            _class=attrs.get('_class').id
        ).exists()
        if self.user_class:
            raise serializers.ValidationError({
                'user': 'Student already added'
            })
        return attrs


class AttendanceSerializer(serializers.ModelSerializer):
    user_class = UserClassSerializer()

    class Meta:
        model = Attendance
        fields = ('id', 'user_class',  'date')


class AttendanceCreateUpdateSerializer(serializers.ModelSerializer):
    user_class = serializers.PrimaryKeyRelatedField(
        queryset=UserClass.objects.all()
    )
    date = serializers.DateField()

    class Meta:
        model = Attendance
        fields = ('id', 'user_class',  'date')

    def __init__(self, instance=None, data=..., **kwargs):
        super().__init__(instance, data, **kwargs)
        self.attendance = None

    def validate(self, attrs):
        self.attendance = Attendance.objects.filter(
            user_class=attrs.get('user_class').id,
            date=attrs.get('date')
        ).exists()
        if self.attendance:
            raise serializers.ValidationError({
                'user': 'Student already attended'
            })
        return attrs


class UserJoinClassSerializer(serializers.Serializer):
    code = serializers.CharField()

    def __init__(self, instance=None, data=..., **kwargs):
        super().__init__(instance, data, **kwargs)
        self.user = None
        self._class = None
        self.user_class = None

    def validate(self, attrs):
        self.user = self.context['request'].user
        self._class = Class.objects.get(code=attrs.get('code'))
        self.user_class = UserClass.objects.filter(
            _class=self._class.id,
            student=self.user.id
        ).exists()
        if self.user_class:
            raise serializers.ValidationError({
                'class': 'User already on the class'
            })
        return attrs

    def create(self, validated_data):
        user_class = UserClass()
        user_class.student = self.user
        user_class._class = self._class
        user_class.save()
        return user_class
