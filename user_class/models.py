import random
import string
from django.db import models


class Class(models.Model):
    class_type = models.CharField(max_length=255)
    code = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @staticmethod
    def generate_class_code(class_type, size=3):
        prefix = "FST60"
        class_year = class_type.split(' ')[1][-2:]
        random_digits = ''.join(
            random.choice(string.digits) for _ in range(size)
        )
        return (prefix
                + class_year
                + random_digits)

    def save(self, *args, **kwargs):
        self.code = self.generate_class_code(self.class_type)
        return super(Class, self).save(*args, **kwargs)


class UserClass(models.Model):
    student = models.ForeignKey(
        to='user.User',
        on_delete=models.CASCADE
    )
    _class = models.ForeignKey(
        to=Class,
        on_delete=models.CASCADE
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Attendance(models.Model):
    user_class = models.ForeignKey(
        to=UserClass,
        on_delete=models.CASCADE
    )
    date = models.DateField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
