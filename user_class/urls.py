from django.urls import path
from rest_framework import routers

from .views import (
    ClassViewSet,
    UserClassViewSet,
    AttendanceViewSet
)

router = routers.DefaultRouter(trailing_slash=False)
router.register('user', UserClassViewSet, basename='class-user')
router.register('attendance', AttendanceViewSet, basename='class-attendance')
router.register('', ClassViewSet, basename='class')

urlpatterns = [
] + router.urls
