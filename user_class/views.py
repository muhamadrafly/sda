from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated

from .models import Class, UserClass, Attendance
from .serializers import (
    ClassSerializer,
    UserClassSerializer,
    UserClassCreateUpdateSerializer,
    AttendanceSerializer,
    AttendanceCreateUpdateSerializer
)


class ClassViewSet(ModelViewSet):
    queryset = Class.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = ClassSerializer


class UserClassViewSet(ModelViewSet):
    queryset = UserClass.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = UserClassSerializer

    def get_serializer_class(self):
        if self.action in ['create', 'update', 'partial_update']:
            return UserClassCreateUpdateSerializer
        return super().get_serializer_class()


class AttendanceViewSet(ModelViewSet):
    queryset = Attendance.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = AttendanceSerializer

    def get_serializer_class(self):
        if self.action in ['create', 'update', 'partial_update']:
            return AttendanceCreateUpdateSerializer
        return super().get_serializer_class()
