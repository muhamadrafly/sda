from django.db import models


class Course(models.Model):
    name = models.CharField(max_length=255)
    room_name = models.CharField(max_length=255, null=True)
    capacity = models.IntegerField(null=True)
    teacher = models.ForeignKey(
        to="user.User",
        on_delete=models.CASCADE,
    )
    _class = models.ForeignKey(
        to="user_class.Class",
        on_delete=models.CASCADE,
    )
    sks = models.IntegerField(null=True)
    begin_time = models.DateTimeField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Grade(models.Model):
    student = models.ForeignKey(
        to="user.User",
        on_delete=models.CASCADE
    )
    course = models.ForeignKey(
        to=Course,
        on_delete=models.CASCADE
    )
    value = models.DecimalField(max_digits=5, decimal_places=2)
    grade = models.CharField(max_length=1)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        if self.value >= 80:
            self.grade = 'A'
        elif self.value >= 65:
            self.grade = 'B'
        elif self.value >= 50:
            self.grade = 'C'
        else:
            self.grade = 'D'
        return super(Grade, self).save(*args, **kwargs)


class Assignment(models.Model):
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    course = models.ForeignKey(
        to=Course,
        on_delete=models.CASCADE
    )
    deadline = models.DateTimeField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Submission(models.Model):
    attachment = models.FileField()
    student = models.ForeignKey(
        to='user.User',
        on_delete=models.CASCADE
    )
    assignment = models.ForeignKey(
        to=Assignment,
        on_delete=models.CASCADE
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
