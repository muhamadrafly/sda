from rest_framework import serializers
from .models import Course, Grade, Assignment, Submission

from user.models import User
from user.serializers import UserSerializer
from user_class.models import Class
from user_class.serializers import ClassSerializer


class CourseSerializer(serializers.ModelSerializer):
    teacher = UserSerializer()
    _class = ClassSerializer()

    class Meta:
        model = Course
        fields = ('id', 'name', 'sks', 'begin_time',
                  'room_name', 'capacity', 'teacher', '_class')


class CourseCreateUpdateSerializer(serializers.ModelSerializer):
    name = serializers.CharField()
    room_name = serializers.CharField()
    sks = serializers.IntegerField()
    capacity = serializers.IntegerField()
    begin_time = serializers.DateTimeField()
    teacher = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.all()
    )
    _class = serializers.PrimaryKeyRelatedField(
        queryset=Class.objects.all()
    )

    class Meta:
        model = Course
        fields = ('name', 'sks', 'begin_time', 'room_name',
                  'capacity', 'teacher', '_class')


class GradeSerializer(serializers.ModelSerializer):
    student = UserSerializer()
    course = CourseSerializer()

    class Meta:
        model = Grade
        fields = ('id', 'student', 'course', 'value', 'grade')


class GradeCreateUpdateSerializer(serializers.ModelSerializer):
    student = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.all()
    )
    course = serializers.PrimaryKeyRelatedField(
        queryset=Course.objects.all()
    )
    value = serializers.DecimalField(max_digits=5, decimal_places=2)
    grade = serializers.CharField(read_only=True)

    class Meta:
        model = Grade
        fields = ('student', 'course', 'value', 'grade')

    def validate(self, attrs):
        grade = Grade.objects.filter(
            course=attrs.get('course').id,
            student=attrs.get('student').id,
        ).exists()
        if grade:
            raise serializers.ValidationError({
                'grade': "Student already graded"
            })
        return attrs


class AssignmentSerializer(serializers.ModelSerializer):
    course = CourseSerializer()

    class Meta:
        model = Assignment
        fields = ('id', 'name', 'description', 'course', 'deadline')


class AssignmentCreateUpdateSerializer(serializers.ModelSerializer):
    name = serializers.CharField()
    description = serializers.CharField(max_length=1000)
    deadline = serializers.DateTimeField()
    course = serializers.PrimaryKeyRelatedField(
        queryset=Course.objects.all()
    )

    class Meta:
        model = Assignment
        fields = ('id', 'name', 'description', 'deadline', 'course')


class SubmissionSerializer(serializers.ModelSerializer):
    student = UserSerializer()
    assignment = AssignmentSerializer()

    class Meta:
        model = Submission
        fields = ('id', 'attachment', 'student', 'assignment')


class SubmissionCreateUpdateSerializer(serializers.ModelSerializer):
    attachment = serializers.FileField()
    student = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.all()
    )
    assignment = serializers.PrimaryKeyRelatedField(
        queryset=Assignment.objects.all()
    )

    class Meta:
        model = Submission
        fields = ('id', 'attachment', 'student', 'assignment')
