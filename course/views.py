from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated

from .models import Course, Grade, Assignment, Submission
from .serializers import (
    CourseSerializer, CourseCreateUpdateSerializer,
    GradeSerializer, GradeCreateUpdateSerializer,
    AssignmentSerializer, AssignmentCreateUpdateSerializer,
    SubmissionSerializer, SubmissionCreateUpdateSerializer,
)


class CourseViewSet(ModelViewSet):
    queryset = Course.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = CourseSerializer

    def get_serializer_class(self):
        if self.action in ['create', 'update', 'partial_update']:
            return CourseCreateUpdateSerializer
        return super().get_serializer_class()


class GradeViewSet(ModelViewSet):
    queryset = Grade.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = GradeSerializer

    def get_serializer_class(self):
        if self.action in ['create', 'update', 'partial_update']:
            return GradeCreateUpdateSerializer
        return super().get_serializer_class()


class AssignmentViewSet(ModelViewSet):
    queryset = Assignment.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = AssignmentSerializer

    def get_serializer_class(self):
        if self.action in ['create', 'update', 'partial_update']:
            return AssignmentCreateUpdateSerializer
        return super().get_serializer_class()


class SubmissionViewSet(ModelViewSet):
    queryset = Submission.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = SubmissionSerializer

    def get_serializer_class(self):
        if self.action in ['create', 'update', 'partial_update']:
            return SubmissionCreateUpdateSerializer
        return super().get_serializer_class()
