from django.urls import path
from rest_framework import routers

from .views import (
    CourseViewSet,
    GradeViewSet,
    AssignmentViewSet,
    SubmissionViewSet
)

router = routers.DefaultRouter(trailing_slash=False)
router.register('assignment', AssignmentViewSet, basename='course-assignment')
router.register('submission', SubmissionViewSet, basename='course-submission')
router.register('grade', GradeViewSet, basename='course-grade')
router.register('', CourseViewSet, basename='course')

urlpatterns = [
] + router.urls
