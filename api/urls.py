from django.urls import path, include


urlpatterns = [
    path('user/',  include(('user.urls', 'user'), namespace='user')),
    path('course/',  include(('course.urls', 'course'), namespace='course')),
    path('class/',  include(('user_class.urls', 'class'), namespace='class')),
    path('exam/',  include(('exam.urls', 'exam'), namespace='exam')),
]
