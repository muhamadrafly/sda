from django.urls import path
from rest_framework import routers

from .views import (
    RegistrationAPIView,
    LoginAPIView,
    UserRetrieveUpdateAPIView,
    UserListAPIView,
    UserClassAPIView,
    UserJoinViewSet
)

router = routers.DefaultRouter(trailing_slash=False)
router.register('class-join', UserJoinViewSet, basename='user-reward')

urlpatterns = [
    path('list', UserListAPIView.as_view()),
    path('class', UserClassAPIView.as_view()),
    path('registration', RegistrationAPIView.as_view()),
    path('login', LoginAPIView.as_view()),
    path('', UserRetrieveUpdateAPIView.as_view()),

] + router.urls
