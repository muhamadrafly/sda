from django.db import models


class Exam(models.Model):
    name = models.CharField(max_length=255)
    teacher = models.ForeignKey(
        to='user.User',
        on_delete=models.CASCADE
    )
    course = models.ForeignKey(
        to='course.Course',
        on_delete=models.CASCADE
    )
    start_date = models.DateTimeField(null=True)
    end_date = models.DateTimeField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Question(models.Model):
    exam = models.ForeignKey(
        to=Exam,
        on_delete=models.CASCADE
    )
    question = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Option(models.Model):
    question = models.ForeignKey(
        to=Question,
        on_delete=models.CASCADE
    )
    option = models.CharField(max_length=255)
    is_answer = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Answer(models.Model):
    exam = models.ForeignKey(
        to=Exam,
        on_delete=models.CASCADE
    )
    student = models.ForeignKey(
        to='user.User',
        on_delete=models.CASCADE
    )
    question = models.ForeignKey(
        to=Question,
        on_delete=models.CASCADE
    )
    option = models.ForeignKey(
        to=Option,
        on_delete=models.CASCADE
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
