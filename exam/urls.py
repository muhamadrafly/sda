from django.urls import path
from rest_framework import routers

from .views import (
    ExamViewSet,
    QuestionViewSet,
    OptionViewSet,
    AnswerViewSet
)

router = routers.DefaultRouter(trailing_slash=False)
router.register('question', QuestionViewSet, basename='question')
router.register('option', OptionViewSet, basename='option')
router.register('answer', AnswerViewSet, basename='answer')
router.register('', ExamViewSet, basename='course')

urlpatterns = [
] + router.urls
