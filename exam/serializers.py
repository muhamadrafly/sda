from rest_framework import serializers
from .models import Exam, Question, Option, Answer

from user.models import User
from course.models import Course

from user.serializers import UserSerializer
from course.serializers import CourseSerializer


class ExamSerializer(serializers.ModelSerializer):
    teacher = UserSerializer()
    course = CourseSerializer()

    class Meta:
        model = Exam
        fields = ('id', 'name', 'start_date', 'end_date', 'teacher', 'course')


class ExamCreateUpdateSerializer(serializers.ModelSerializer):
    name = serializers.CharField()
    start_date = serializers.DateTimeField()
    end_date = serializers.DateTimeField()
    teacher = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.all()
    )
    course = serializers.PrimaryKeyRelatedField(
        queryset=Course.objects.all()
    )

    class Meta:
        model = Exam
        fields = ('id', 'name', 'start_date', 'end_date', 'teacher', 'course')


class QuestionSerializer(serializers.ModelSerializer):
    exam = ExamSerializer()

    class Meta:
        model = Question
        fields = ('id', 'question', 'exam')


class QuestionCreateUpdateSerializer(serializers.ModelSerializer):
    question = serializers.CharField()
    exam = serializers.PrimaryKeyRelatedField(
        queryset=Exam.objects.all()
    )

    class Meta:
        model = Question
        fields = ('id', 'question', 'exam')


class OptionSerializer(serializers.ModelSerializer):
    question = QuestionSerializer()

    class Meta:
        model = Option
        fields = ('id', 'option', 'is_answer', 'question')


class OptionCreateUpdateSerializer(serializers.ModelSerializer):
    question = serializers.PrimaryKeyRelatedField(
        queryset=Question.objects.all()
    )
    option = serializers.CharField()
    is_answer = serializers.BooleanField()

    class Meta:
        model = Option
        fields = ('id', 'option', 'is_answer', 'question')


class AnswerSerializer(serializers.ModelSerializer):
    exam = ExamSerializer()
    student = UserSerializer()
    question = QuestionSerializer()
    option = OptionSerializer()

    class Meta:
        model = Answer
        fields = ('id', 'exam', 'student', 'question', 'option')


class AnswerCreateUpdateSerializer(serializers.ModelSerializer):
    exam = serializers.PrimaryKeyRelatedField(
        queryset=Exam.objects.all()
    )
    question = serializers.PrimaryKeyRelatedField(
        queryset=Question.objects.all()
    )
    option = serializers.PrimaryKeyRelatedField(
        queryset=Option.objects.all()
    )

    class Meta:
        model = Answer
        fields = ('id', 'exam', 'question', 'option', 'student')

    def create(self, validated_data):
        answer = Answer()
        answer.exam = validated_data.get('exam')
        answer.question = validated_data.get('question')
        answer.option = validated_data.get('option')
        answer.student = self.context['request'].user
        answer.save()
        return answer
