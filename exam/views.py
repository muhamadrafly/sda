from rest_framework.viewsets import ModelViewSet, GenericViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework import status

from .models import Exam, Question, Option, Answer
from .serializers import (
    ExamSerializer, ExamCreateUpdateSerializer,
    QuestionSerializer, QuestionCreateUpdateSerializer,
    OptionSerializer, OptionCreateUpdateSerializer,
    AnswerSerializer, AnswerCreateUpdateSerializer,
)


class ExamViewSet(ModelViewSet):
    queryset = Exam.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = ExamSerializer

    def get_serializer_class(self):
        if self.action in ['create', 'update', 'partial_update']:
            return ExamCreateUpdateSerializer
        return super().get_serializer_class()


class QuestionViewSet(ModelViewSet):
    queryset = Question.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = QuestionSerializer

    def get_serializer_class(self):
        if self.action in ['create', 'update', 'partial_update']:
            return QuestionCreateUpdateSerializer
        return super().get_serializer_class()

    @action(methods=['GET'], detail=True)
    def exam(self, request, pk, *args, **kwargs):
        instance = self.get_queryset().filter(exam_id=pk)
        serializer = self.get_serializer(instance=instance, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class OptionViewSet(ModelViewSet, GenericViewSet):
    queryset = Option.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = OptionSerializer

    def get_serializer_class(self):
        if self.action in ['create', 'update', 'partial_update']:
            return OptionCreateUpdateSerializer
        return super().get_serializer_class()

    @action(methods=['GET'], detail=True)
    def question(self, request, pk, *args, **kwargs):
        instance = self.get_queryset().filter(question_id=pk)
        serializer = self.get_serializer(instance=instance, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class AnswerViewSet(ModelViewSet):
    queryset = Answer.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = AnswerSerializer

    def get_serializer_class(self):
        if self.action in ['create', 'update', 'partial_update']:
            return AnswerCreateUpdateSerializer
        return super().get_serializer_class()

    @action(methods=['GET'], detail=True)
    def exam(self, request, pk, *args, **kwargs):
        instance = self.get_queryset().filter(exam_id=pk)
        serializer = self.get_serializer(instance=instance, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(methods=['GET'], detail=True)
    def question(self, request, pk, *args, **kwargs):
        instance = self.get_queryset().filter(question_id=pk)
        serializer = self.get_serializer(instance=instance, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
